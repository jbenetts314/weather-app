//
//  WeatherModelAPI.swift
//  WeatherApp
//
//  Created by jbenetts on 3/28/22.
//

import Foundation


// MARK: - Weather
struct OneCallWeather: Decodable {
    let lat, lon: Double
    let timezone: String
    let daily: [Daily] //Array
}

// MARK: - Daily
struct Daily: Decodable {
    let dt: Int
    let temp: Temp
    let weather: [WeatherElement]
}

// MARK: - Temp
struct Temp: Decodable {
    let day, min, max: Double
}

// MARK: - WeatherElement
struct WeatherElement: Decodable {
    let id: Int
    let main: WeatherType
    let description, icon: String
}
