//
//  WeatherModels.swift
//  WeatherApp
//
//  Created by jbenetts on 3/23/22.
//

import Foundation
import UIKit

//Enum
enum WeatherType : String, Decodable{
    case thunderstorm = "Thunderstorm"
    case drizzle = "Drizzle"
    case rain = "Rain"
    case snow = "Snow"
    case atmosphere = "Atmosphere"
    case clear = "Clear"
    case clouds = "Clouds"
    var image: UIImage {
        switch self {
        case .thunderstorm:
            return UIImage(named: "thunderstorm")!
        case .drizzle:
            return UIImage(named: "fog")!
        case .rain:
            return UIImage(named: "rainy")!
        case .snow:
            return UIImage(named: "snow")!
        case .atmosphere:
            return UIImage(named: "fog")!
        case .clear:
            return UIImage(named: "sun")!
        case .clouds:
            return UIImage(named: "clouds")!
        }
    }
}

//Models
struct ResponseBody: Decodable {
    var coord: CoordinatesAPI
    var weather: [WeatherAPI]
    var main: MainAPI
    var name: String
    var wind: WindAPI
    var sys: SysAPI

    struct CoordinatesAPI: Decodable {
        var lon: Double
        var lat: Double
    }

    struct WeatherAPI: Decodable {
        var id: Double
        var main: WeatherType
        var description: String
        var icon: String
    }

    struct MainAPI: Decodable {
        var temp: Double
        var feels_like: Double
        var temp_min: Double
        var temp_max: Double
        var pressure: Double
        var humidity: Double
    }
    
    struct WindAPI: Decodable {
        var speed: Double
        var deg: Double
    }
    struct CloudAPI: Decodable{
        var all: Double
    }
    
    struct SysAPI: Decodable{
        var type: Double
        var id: Double
        var country: String
        var sunrise: Double
        var sunset: Double
    }
}

//Table
struct WeatherTable{
    let day: String
    let maxTemp: Double
    let minTemp: Double
    let weather: WeatherType
}
