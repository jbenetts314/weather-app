//
//  DetailWeatherController.swift
//  WeatherApp
//
//  Created by jbenetts on 3/30/22.
//
import UIKit
import Foundation

class DetailWeatherController: UIViewController{
    var weather: Daily?
    @IBOutlet var maxMinTempLabel: UILabel?
    @IBOutlet var dayNameLabel: UILabel?
    @IBOutlet var iconWeatherImage: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dayName = formatDate(dt: Double(weather?.dt ?? 0), format: "EEEE")
        dayNameLabel?.text = dayName
        maxMinTempLabel?.text = "\(Int(weather?.temp.max ?? 0))º | \(Int(weather?.temp.min ?? 0) )º"
        iconWeatherImage?.image = weather?.weather.first?.main.image
    }
}
