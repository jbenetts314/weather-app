//
//  ViewController.swift
//  WeatherApp
//
//  Created by jbenetts on 3/18/22.
//
//For location

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    //JSON OneCall response
    var weathersResponseW: [Daily] = []
    //Objects to call the location and current weather functionality
    var locationManager = LocationManager() //To call the location manager
    var weatherManager = WeatherManager() //To call the Weather Manager
    //Tables
    @IBOutlet weak var dailyWeatherTable: UITableView!
    //Labels
    @IBOutlet weak var todayTemperatureLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var minMaxTempLabel: UILabel!
    @IBOutlet weak var mainWeatherLabel: UILabel!
    //Images
    @IBOutlet weak var todayWeatherImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegateLocation = self
        locationManager.setupLocation()
        //Table
        dailyWeatherTable.delegate = self
        dailyWeatherTable.dataSource = self //Is in charge to write de info
    }
    //This one tell the table how many rows will have
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weathersResponseW.count
    }
    //This one writes de information that the table need
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DayCell", for: indexPath)
        let dayRow = weathersResponseW[indexPath.row]
        let dayName = formatDate(dt: Double(dayRow.dt), format: "EE")
        cell.textLabel?.text = "\(dayName)    \(Int(dayRow.temp.max))º | \(Int(dayRow.temp.min))º"
        let type = dayRow.weather[indexPath.startIndex].main
        cell.imageView?.image = type.image
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "SegwailToWeatherDetail", sender: nil) //enum en identifier
//        guard let dayIndex = indexPath.row else {fatalError("Nothing Selected")}
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) { //segwail in storyboard and navigation with code
        if let viewController = segue.destination as? DetailWeatherController, let index = dailyWeatherTable.indexPathForSelectedRow?.row{
            let dayRow = weathersResponseW[index]
            viewController.weather = dayRow
        }
    }
}

//Get the API Call
extension ViewController: LocationDelegate{
    func updateLocation(lat: Double, long: Double){
        print("Long: \(long), Lat: \(lat)") //De que forma recibir esto de forma asíncrona
        Task.init{
            do{
                let weathersC = try await weatherManager.getCurrentWeather(latitude: lat, longitude: long)
                print("This is the daily weather response\n\(weathersC)")
                setUpWeatherDay(temp: Int(weathersC.main.temp), weatherName: WeatherType(rawValue: weathersC.name) ?? WeatherType.clear, city: weathersC.name, country: weathersC.sys.country, minTemp: Int(weathersC.main.temp_min), maxTemp: Int(weathersC.main.temp_max), description: String(weathersC.weather.description))
                let weathersW = try await weatherManager.getWeeklyWeather(latitude: lat, longitude: long)
                print("This is the OneCall weather response\n\(weathersW)")
                weathersResponseW = weathersW.daily //Remove first
                weathersResponseW.removeFirst()
                DispatchQueue.main.async {
                    self.dailyWeatherTable.reloadData()
                }
            }
            catch{
                print(error)
            }
        }
    }
}

//Extension to set up the current weather
extension ViewController {
    func setUpWeatherDay(temp: Int, weatherName: WeatherType, city: String, country: String, minTemp: Int, maxTemp: Int, description: String){
        todayTemperatureLabel.text = "\(temp)º"
        locationLabel.text = "\(city), \(country)"
        minMaxTempLabel.text = "HIGH: \(maxTemp)º | LOW: \(minTemp)º"
        mainWeatherLabel.text = "\(weatherName)".uppercased()
        todayWeatherImage.image = weatherName.image
    }
}
//Function to format the DT from JSON call
func formatDate(dt: Double, format: String) -> String{
    let date = Date(timeIntervalSince1970: dt)
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = format
    let dayName = dateFormatterGet.string(from: date)
    return dayName
}
