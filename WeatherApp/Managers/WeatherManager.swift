//
//  WeatherManager.swift
//  WeatherApp
//
//  Created by jbenetts on 3/22/22.
// This is created to manage all the weather API things we need

import Foundation

class WeatherManager{
    func getCurrentWeather(latitude: Double, longitude: Double) async throws -> ResponseBody{ //This one returns only for todays weather
        //For the Current Weather
        let urlAPI = "https://api.openweathermap.org/data/2.5/weather?lat=\(latitude)&lon=\(longitude)&appid=06e971855e91d8dff737956d1b165571&units=metric"
        guard let urlCall = URL(string: urlAPI)
        else{ //NOTE: guard is like an if but it does the condition when is not met
            fatalError("No URL")
        }   
        let urlRequest = URLRequest(url: urlCall) //Make the request with de URL
        let (data, response) = try await URLSession.shared.data(for: urlRequest)
        guard (response as? HTTPURLResponse)?.statusCode == 200 else { fatalError("Error getting the data")}
        let jsonDecoded = try JSONDecoder().decode(ResponseBody.self, from: data) //save all the data on the Response Body we make down
        return jsonDecoded
    }
    //For the Weekly Weather
    func getWeeklyWeather(latitude: Double, longitude: Double) async throws -> OneCallWeather{ //This one returns only for todays weather
        let urlAPI = "https://api.openweathermap.org/data/2.5/onecall?lat=\(latitude)&lon=\(longitude)&exclude=minutely,hourly,alerts,current&appid=06e971855e91d8dff737956d1b165571&units=metric"
        guard let urlCall = URL(string: urlAPI)
        else{ //NOTE: guard is like an if but it does the condition when is not met
            fatalError("No URL")
        }
        let urlRequest = URLRequest(url: urlCall) //Make the request with de URL
        let (data, response) = try await URLSession.shared.data(for: urlRequest)
        guard (response as? HTTPURLResponse)?.statusCode == 200 else { fatalError("Error getting the data")}
        let jsonDecoded = try JSONDecoder().decode(OneCallWeather.self, from: data) //save all the data on the Response Body we make down
        return jsonDecoded
    }
}
