//
//  LocationManager.swift
//  WeatherApp
//
//  Created by jbenetts on 3/18/22.
// This is created to manage all the location things we need

import Foundation
import CoreLocation

typealias LocationCoordinates = (lat: Double, lon: Double)

class LocationManager: NSObject, CLLocationManagerDelegate{
    let locationManagerM = CLLocationManager()
    var currentLocation: CLLocation?
    //Define Delegate locationmanager on ViewController
    weak var delegateLocation: LocationDelegate? //Investigate weak, strong, unknown
    func setupLocation(){
        locationManagerM.delegate = self
        locationManagerM.requestWhenInUseAuthorization()
        locationManagerM.startUpdatingLocation()
    }

    //To To return error NOTE: Check protocols for errors
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("locationManager error:\n \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !locations.isEmpty, currentLocation == nil{ //To ensure is empthy
            currentLocation = locations.first
            //locationManagerM.stopUpdatingLocation() //To save baterry, we stop the location
            guard let currentLocations = getCurrentLocation() else  {
                return
            }
            delegateLocation?.updateLocation(lat: currentLocations.lat, long: currentLocations.lon)
        } else{
            print("The user denied the location")
        }
    }

    //To display the location setupLocation()
    @discardableResult func getCurrentLocation() -> LocationCoordinates? {
        guard let currentLocation = currentLocation else { //NOTE: guard is like an if but it does the condition when is not met
            return nil
        }
        let longitudeUser = currentLocation.coordinate.longitude
        let latitudeUser = currentLocation.coordinate.latitude
        print("Longitude: \(longitudeUser) | Latitude: \(latitudeUser)")
        return LocationCoordinates(lat: latitudeUser, lon: longitudeUser)
    }
}
