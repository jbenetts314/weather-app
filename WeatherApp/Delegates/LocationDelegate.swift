//
//  LocationDelegate.swift
//  WeatherApp
//
//  Created by jbenetts on 3/22/22.
//

import Foundation

protocol LocationDelegate : AnyObject{
    func updateLocation(lat: Double, long: Double)
}
